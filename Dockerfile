FROM heroku/nodejs

RUN apt-get update -y && apt-get install -y redis-server

# Create app directory
WORKDIR /usr/src/app

COPY package*.json ./

# Install all packages
RUN npm install

# Bundle app source
COPY . .
COPY entrypoint.sh /usr/local/bin/

RUN adduser -D myuser
USER myuser

EXPOSE 8088
ENTRYPOINT [ "entrypoint.sh" ]
CMD [ "npm", "start" ]
